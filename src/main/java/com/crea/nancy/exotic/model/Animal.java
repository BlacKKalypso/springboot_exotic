package com.crea.nancy.exotic.model;

import javax.persistence.Id;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "crea", type = "animal")
/**
 * animal
 */
public class Animal {

  @Id
  @Field(type = FieldType.Long)
  private int id;

  @Field(type = FieldType.Text)
  private String name;

  @Field(type = FieldType.Text)
  private String natEnvironnement;

  @Field(type = FieldType.Text)
  private String image;

  public Animal() {
    super();
  }

  public Animal(int id, String name, String natEnvironnement, String image) {
    this.id = id;
    this.name = name;
    this.natEnvironnement = natEnvironnement;
    this.image = image;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNatEnvironnement() {
    return natEnvironnement;
  }

  public void setNatEnvironnement(String natEnvironnement) {
    this.natEnvironnement = natEnvironnement;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }
}