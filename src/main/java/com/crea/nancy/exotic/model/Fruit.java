package com.crea.nancy.exotic.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Base de données h2 / MYSQL
 */
@Entity
@Table(name = "fruit")
/**
 * fruit
 */
public class Fruit {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  private String name;
  private String origin;
  private String image;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public Fruit() {
  }

  public Fruit(int id, String name, String origin, String image) {
    this.id = id;
    this.name = name;
    this.origin = origin;
    this.image = image;
  }

}