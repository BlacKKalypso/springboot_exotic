package com.crea.nancy.exotic.controller;

import java.util.ArrayList;

import com.crea.nancy.exotic.dao.AnimalDao;
import com.crea.nancy.exotic.model.Animal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

/**
 * AnimalController
 */
@Api(description = "API pour les opérations CRUD sur les produits.")
@RestController("/")
public class AnimalController {

  @Autowired
  private AnimalDao animalDao;

  // Recherche Animal par le nom
  @GetMapping(value = "/Animal/{name}")
  public Animal showOneAnimalName(@PathVariable String name) {
    return animalDao.findByName(name);
  }

  // Recherche Animal par l'environnement naturel
  @GetMapping(value = "/Animal/{natEnv}")
  public ArrayList<Animal> showAnimalNatEnv(@PathVariable String natEnv) {
    return animalDao.findByEnv(natEnv);
  }

  // Afficher tous les produits
  @RequestMapping(value = "/AllAnimal", method = RequestMethod.GET)
  public Page<Animal> showAllAnimal() {
    return animalDao.findAll();
  }
}