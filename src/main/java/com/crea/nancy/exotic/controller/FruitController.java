package com.crea.nancy.exotic.controller;

import java.util.List;

import com.crea.nancy.exotic.dao.FruitDao;
import com.crea.nancy.exotic.model.Fruit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;

/**
 * FruitController
 */
@Api(description = "API pour les opérations CRUD sur les fruits")
@RestController("/Fruit")
public class FruitController {
  @Autowired
  private FruitDao fruitDao;
  
  // Recherche fruit par le nom
  @GetMapping(value = "/Fruit/{name}")
  public Fruit showFruitName(@PathVariable String name) {
    return fruitDao.findByName(name);
  }

  // Afficher tous les fruits
  @RequestMapping(value = "/AllFruit", method = RequestMethod.GET)
  public List<Fruit> showAllFruits() {
    return fruitDao.findAll();
  }
}