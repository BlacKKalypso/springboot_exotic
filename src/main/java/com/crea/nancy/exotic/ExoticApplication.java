package com.crea.nancy.exotic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExoticApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExoticApplication.class, args);
	}

}
