package com.crea.nancy.exotic.dao;

import java.util.ArrayList;

import com.crea.nancy.exotic.model.Animal;

import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

/**
 * AnimalDao
 */
@Repository
public interface AnimalDao extends ElasticsearchRepository<Animal, Integer> {
  public Page<Animal> findAll();

  public Animal findById(int id);

  public Animal findByName(String Name);

  public ArrayList<Animal> findByEnv(String natEnv);

}