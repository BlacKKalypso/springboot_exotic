package com.crea.nancy.exotic.dao;

import java.util.List;

import com.crea.nancy.exotic.model.Fruit;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * FruitDao
 */
@Repository
public interface FruitDao extends JpaRepository<Fruit, Integer> {
  public List<Fruit> findAll();

  public Fruit findById(int id);

  public Fruit findByName(String name);

  public List<Fruit> findByOrigin(String origin);

}