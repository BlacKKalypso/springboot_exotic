# Springboot + Elasticsearch Animal 

Utilisation de Springboot et d'Elasticsearch pour créer une base de données de pokémons.

## Création du projet

Télécharger le projet Springboot
* [Spingboot](https://start.spring.io/) - Projet de base
* Project: Gradle
* Language: Java
* Springboot : 2.1.8.RELEASE

Télécharger un JDK
* [JDK](https://developers.redhat.com/products/openjdk/download?sc_cid=701f2000000RWTnAAO)

### Architecture du projet
Dans ce dossier : src/main/java/com/crea/exemple/projet

Créer les dossiers : controller , dao  et model

* Créer les models
* Créer les controllers
* Créer les Dao

## CHOIX 1 : Connexion à une base de donnée type h2 / MySql
Dans ce dossier : src/main/ressources
* Créer un fichier data sql pour alimenter votre base de données : 
``` 
INSERT INTO fruit(id, name, origin, image) VALUES(1, 'Kiwi', 'Chine', 'https://en.wikipedia.org/wiki/File:Kiwi1.1.jpg');
```
! Attention à respecter l'ordre des données que vous avez mis dans vos models.

## CHOIX 2 : Connexion à une base de donnée Elasticsearch
### Dépendances + Installation
Ajouter les dépendance dans votre build.gradle 
```
implementation 'org.springframework.boot:spring-boot-starter-data-elasticsearch'
```

Installer Elasticsearch sur votre ordinateur
* [Elasticsearch](https://www.elastic.co/fr/start?ultron=[EL]-[B]-[EMEA-General]-Exact&blade=adwords-s&Device=c&thor=install%20elasticsearch&gclid=Cj0KCQiAoIPvBRDgARIsAHsCw09xo3GelaxujSuKIF-PE_oJ1eyNR0mZsoqdRmGCuH-e_DyKvVgT0ikaArK2EALw_wcB)

Vous pouvez aussi installer kibana ce qui vous permettera d'avoir une interface graphic à Elastic
* [Kibana](https://www.elastic.co/fr/webinars/getting-started-kibana?elektra=startpage)

### Lier le Springboot + Elastic
Créer un index (ex: crea) qui va accueillir vos données dans Elastic. 
Il faut ajouter une ligne dans votre model que vous souhaitez utiliser (ex: Animal.java)
```
@Document(indexName = "crea", type = "animal")
public class Animal {

  @Id
  @Field(type = FieldType.Long)
  private int id;

  @Field(type = FieldType.Text)
  private String name;

  ...
}

```

Et connecter votre model à Elastic avec votre Dao (ex: AnimalDao.java)

```
@Repository
public interface AnimalDao extends ElasticsearchRepository<Animal, Integer> {
  public Page<Animal> findAll();
  public Animal findById(int id);
  public Animal findByName(String Name);
  public ArrayList<Animal> findByEnv(String natEnv);

}

```

Créer un fichier JSON avec les données que vous souhaitez injecter dans la base de donnée

```
{"index":{"_index": "crea", "_type": "animal", "_id": 3}}
{"id":"3","name":"Girafe","natEnvironnement":"Afrique","image":"https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Giraffa_camelopardalis_angolensis.jpg/435px-Giraffa_camelopardalis_angolensis.jpg"}
```
Lancer votre Elastic depuis le dossier elastic
```
bin/elasticsearch
```
Builder votre projet springbooter avec gradlew

Injecter les données depuis le dossier ou vous avez le json
```
curl -XPUT localhost:9200/_bulk -H "Content-Type: application/json" --data-binary @animal.json
```

Killer et relancer votre elastic et votre kibana
```
bin/elasticsearch
```
```
bin/kibana
```

##C'est fini